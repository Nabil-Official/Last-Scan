#!/usr/bin/python
import os,requests
from src import logo,api_scan,default_scan

class Main:
    def __init__(self):
        __version__ = "0.1"
        try:
          req = requests.get("https://gitlab.com/Nabil-Official/Last-Scan/-/raw/main/version%20").text.strip("\n")
        except:
          print(f"\033[1;31m[+] \033[1;37mERROR : \033[1;31mCheck Your Internet !!!")
          exit()

        if req != __version__:
           print(f"\033[1;31m[+] \033[1;37mERROR : \033[1;31mNot Updated :(")
           exit()

    def menu(self):
        print("""\033[1;37m[1] \033[1;31m-\033[1;32m-\033[1;34m# \033[1;32mScan Port \033[1;37m(Using Api)
\033[1;37m[2] \033[1;31m-\033[1;32m-\033[1;34m# \033[1;32mScan Port \033[1;37m(Default)
""")
        user = int(input("\033[1;35mL\033[1;37mast\033[1;35mS\033[1;37mcan \033[1;31m>\033[1;32m> \033[1;37m"))

        if user ==  1:
           domain = str(input("\n\033[1;31m[+] \033[1;37mEnter Domain/IP : \033[1;34m"))
           api_scan.scan(domain)

        elif user == 2:
           domain = str(input("\n\033[1;31m[+] \033[1;37mEnter Domain/IP : \033[1;34m"))
           range_ = int(input("\033[1;31m[+] \033[1;37mPort Range : \033[1;34m"))
           default_scan.scan(domain,range(range_))


if __name__ == "__main__":
   os.system("clear")
   logo.logo()
   root = Main()
   root.menu()

