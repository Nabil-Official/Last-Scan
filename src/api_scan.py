import json
import requests
import os

def scan(domain):
    print("\n----------------------------------------")
    print(f"\n          \033[1;31m[+] \033[1;37mScanning : {domain}\n")
    try:
       data = requests.get(f"https://api.viewdns.info/portscan/?host={domain}&apikey=9c0dfb238d64a4dcf14484e686f68ebbd8471333&output=json").text
    except Exception as e:
       print(f"\033[1;31m[+] \033[1;37mERROR : \033[1;31mCheck Your Internet !!!")
       exit()
    j_data = json.loads(data)
    port_n_service = j_data['response']['port']
    for ports in port_n_service:
        if ports['status'] != "closed":
           print(f"\033[1;31m# \033[1;37mPort : \033[1;32m"+ports['number']+' '+'\033[1;31m>> \033[1;37mService : \033[1;32m'+ports['service'])
    print("\n")


